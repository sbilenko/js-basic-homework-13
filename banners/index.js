/*
1. 
setTimeout() - викликається один раз через певний проміжок часу, який ми передаємо другим аргументом.
setInterval() - викликається до тих пір, поки ми не зупинимо за допомогою функції clearInterval().
2.
setTimeout() спрацює миттєво, але буде працювати дуже некоректно.
3.
Тому що,  якщо не викликати функцію clearInterval(), то на сайті можуть відбуватися витоки пам'яті та баги через несподівані спрацьовування раніше написаної функції setInterval()
*/

const imagesWrapper = document.querySelector('.images-wrapper')
const imagesWrapperArray = Array.from(imagesWrapper.children)

const imageWidth = imagesWrapperArray[0].clientWidth

const btnStop = document.querySelector('.button-stop')
const btnStart = document.querySelector('.button-start')

const imagesWrapperWidth = imagesWrapper.clientWidth * imagesWrapperArray.length

const setTimeoutDelay = 3000



let offset = 0
let timer
function run() {
  offset += imageWidth 
  
  if (offset >= imagesWrapperWidth) {
    offset = 0 
  }

  imagesWrapper.style.left = `${-offset}px`

  timer = setTimeout(run, setTimeoutDelay)
}
setTimeout(run, setTimeoutDelay)



btnStop.addEventListener('click', function() {
  clearTimeout(timer)
})

btnStart.addEventListener('click', function() {
  setTimeout(run, 0)
})